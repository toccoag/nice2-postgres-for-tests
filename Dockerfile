FROM registry.gitlab.com/toccoag/nice2-postgres:15

COPY /dumps/ /dumps/
COPY scripts/* /docker-entrypoint-initdb.d/
COPY config/* /etc/postgresql.d/

# Restore dump during build
#
# This will increase the size of the image considerably.
#
# RUN ( \
#   postgres & \
#   pid=$!; \
#   while ! pg_isready -U nice -d nice; do sleep 1; done \
#     && pg_restore -U nice -d nice --no-owner --no-acl -j 4 /dumps/dump.psql \
#     && psql -v ON_ERROR_STOP=1 -U postgres -d nice -c ANALYZE \
#     && kill $pid \
#     && wait \
#     && touch /data/NICE_DB_RESTORED
# )
